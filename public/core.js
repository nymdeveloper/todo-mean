/**
 * Description of file.
 * @author: adalto.junior
 * Date: 16/11/13
 * Time: 16:54
 */

var scotchTodo  = angular.module('scotchTodo', []);

function mainController ($scope, $http) {

    $scope.formData = {};

    $http.get('api/todos')
        .success(function (data) {

            $scope.todos = data;

        })
        .error (function (data){
            console.log('Error: ' + data);
        });

    $scope.createTodo = function() {
        $http.post('/api/todos', $scope.formData)
            .success(function(data) {
                $('input').val('');
                $scope.todos = data;
                console.log(data);
            })
            .error(function(data) {
                console.log('Error: ' + data);
            });
    };

    // delete a todo after checking it
    $scope.deleteTodo = function(id) {
        $http.delete('/api/todos/' + id)
            .success(function(data) {
                $scope.todos = data;
                console.log(data);
            })
            .error(function(data) {
                console.log('Error: ' + data);
            });
    };

}
