
/**
 * Module dependencies.
 */

var express  = require('express');
var app      = express(); 								// create our app w/ express
var mongoose = require('mongoose'); 					// mongoose for mongodb

var port = process.env.PORT || 8080;

mongoose.connect('mongodb://node:node@mongo.onmodulus.net:27017/uwO3mypu'); 	// connect to mongoDB database on modulus.io

app.configure(function() {
    app.use(express.static(__dirname + '/public')); 		// set the static files location /public/img will be /img for users
    app.use(express.logger('dev')); 						// log every request to the console
    app.use(express.bodyParser()); 							// pull information from html in POST
    app.use(express.methodOverride()); 						// simulate DELETE and PUT
});


var Todo = mongoose.model('Todo', {
    text : String,
    done : Boolean
});


app.get('/api/todos', function (req, res) {

    Todo.find(function (err, todos) {

        if (err) {
            res.send(err);
        }
            console.log(todos);
        res.json(todos);

    });

});

app.post('/api/todos', function (req, res) {

    Todo.create({
        text: req.body.text,
        done: false
    }, function (err, todo) {

        if (err) {

            res.send(err);

        }

        Todo.find(function (err, todos) {

            if (err) {
                res.send(err);
            }

            res.json(todos);

        });

    });

});

app.delete('/api/todos/:id', function (req, res) {

    Todo.remove({
        _id: req.params.id
    }, function (err, todo) {

        if (err) {

            res.send(err);

        }

        Todo.find(function (err, todos) {

            if (err) {
                res.send(err);
            }

            res.json(todos);

        });
    });

});

app.get('*', function (req, res) {

    res.sendfile('./public/index.html');

});

app.listen(port);
console.log("App listening on port " + port);

